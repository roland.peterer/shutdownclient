# README

## Setup

```shell
# install poetry
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3 -

# install dependencies
poetry install

# gpiozero needs reboot
sudo reboot 

# copy service file 
sudo cp shutdown.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable shutdown.service
sudo systemctl start shutdown.service
sudo systemctl status shutdown.service
# check message in log service
journalctl -u shutdown.service -f
```

## Pinout

![](./assets/pin_layout.svg)