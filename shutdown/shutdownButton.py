from gpiozero import Button
import os

PIN_NUMBER = 5

shutdown_button = Button(PIN_NUMBER, pull_up=True)


def shutdown():
    os.system("sudo shutdown now --poweroff")


def main():
    print("shutting system down...")
    shutdown_button.wait_for_press()
    shutdown()


if __name__ == "__main__":
    main()
